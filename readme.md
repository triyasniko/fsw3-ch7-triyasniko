
# Challenge Binar Chapter 7

Repo ini berisi project binar pada chapter ke 7


## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`React Js`

`Express Js`


## Installation & Running

Menjalankan Server

```bash
  cd server
  npm install
  node app.js
```
Install Client

```bash
  cd client
  npm install
  npm start
```